#!/bin/bash
#SBATCH --job-name=repeatModeler
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o t1_%j.out
#SBATCH -e t1_%j.err

module load RepeatModeler/1.0.8
module load rmblastn/2.2.28
module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/

BuildDatabase -engine ncbi -name P.africana /projects/EBP/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/new_annotation/RepeatModeler/final_assembly.fa

RepeatModeler -engine ncbi -pa 8 -database P.africana
