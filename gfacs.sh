#!/bin/bash
#SBATCH --job-name=gFACs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=10G
#SBATCH --mail-user=nasim.rahmarpour@uconn.com
#SBATCH -o gfacs-%j.o
#SBATCH -e gfacs-%j.e

module load perl/5.28.1

org="/projects/EBP/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/new_annotation/gFACs/"
genome="/projects/EBP/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/new_annotation/RepeatMasker/final_assembly.fa.masked"
alignment="/projects/EBP/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/new_annotation/gFACs/augustus.hints.gff3"

script="/labs/Wegrzyn/gFACs/gFACs.pl"

if [ ! -d mono_o ]; then
        mkdir mono_o multi_o
fi
if [ ! -d multi_o ]; then
        mkdir multi_o
fi

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--rem-multiexonics \
--rem-all-incompletes \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--min-CDS-size 300 \
--get-protein-fasta \
--fasta "$genome" \
-O mono_o \
"$alignment"

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--rem-monoexonics \
--min-exon-size 9 \
--min-intron-size 9 \
--min-CDS-size 300 \
--fasta "$genome" \
-O multi_o \
"$alignment"

cd multi_o
# remove lines with 5_INC+3_INC here
grep '5_INC+3_INC' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > fullinc_ids.txt
grep 'gene' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > gene_table_ids.txt
grep -vxFf fullinc_ids.txt gene_table_ids.txt > comp_inc_ids.txt
python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList comp_inc_ids.txt --idPath . --out comp_inc_gene_table.txt
if [ ! -d start_o ]; then
	mkdir start_o
fi
if [ ! -d stop_o ]; then
	mkdir stop_o
fi

alignment="comp_inc_gene_table.txt"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-start-codon \
--fasta "$genome" \
-O start_o \
"$alignment"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-stop-codon \
--fasta "$genome" \
-O stop_o \
"$alignment"

grep 'gene' start_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' | uniq > starts_ids.txt
grep 'gene' stop_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' |  uniq > stops_ids.txt
grep -vxFf starts_ids.txt comp_inc_ids.txt > missing_starts_ids.txt
grep -vxFf stops_ids.txt comp_inc_ids.txt > missing_stops_ids.txt
grep -xFf missing_starts_ids.txt missing_stops_ids.txt > missing_both_ids.txt
grep -vxFf missing_both_ids.txt comp_inc_ids.txt > completes_partials_ids.txt

python ../filtergFACsGeneTable.py --table comp_inc_gene_table.txt --tablePath . --idList completes_partials_ids.txt --idPath . --out completes_partials_gene_table.txt

cd ../

cat mono_o/gene_table.txt   multi_o/completes_partials_gene_table.txt > mono_multi_gene_table.txt

if [ ! -d final_o ]; then
	mkdir final_o
fi
alignment="mono_multi_gene_table.txt"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--splice-table \
--get-protein-fasta \
--get-fasta-without-introns \
--get-fasta-with-introns \
--create-gff3 \
--create-gtf \
--fasta "$genome" \
-O final_o \
"$alignment"

