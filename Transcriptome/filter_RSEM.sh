#!/bin/bash
#SBATCH --job-name=RSEM-filter
#SBATCH -o RSEM_filter-%j.output
#SBATCH -e RSEM_filter-%j.error
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general

module load trinity/2.6.6

/labs/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_transcriptome/RSEM/cutoff_FPKM0.5/filter_fasta_by_rsem_values.pl \
	--rsem_output=/labs/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_transcriptome/RSEM/Transcriptome.RSEM.isoforms.results \
	--fasta=/labs/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_transcriptome/RSEM/Transcriptome.Trinity.transcripts.fa \
	--output=/labs/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_transcriptome/RSEM/cutoff_FPKM0.5/Transcriptome_fpkm_filtered.fasta \
	--fpkm_cutoff=0.5 \
	--isopct_cutoff=1

