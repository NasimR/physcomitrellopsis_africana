#!/bin/bash
#SBATCH --job-name=Trinity
#SBATCH -o trinity-%j.output
#SBATCH -e trinity-%j.error
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=50G
#SBATCH --partition=general

module load trinity/2.6.6
Trinity --seqType fq --left /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G1/cat.G1.R1.fastq --right /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G1/cat.G1.R2.fastq --min_contig_length 300 --output /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G1/trinity_run --full_cleanup --max_memory 50G --CPU 8
