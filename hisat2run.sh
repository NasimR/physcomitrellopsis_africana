#!/bin/bash
#SBATCH --job-name=hisat2run
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mem=50G
#SBATCH -o hisat2run_%j.out
#SBATCH -e hisat2run_%j.err
module load hisat2
module load samtools
hisat2 -x P.africana_masked -1 paired_1.fq -2 paired_2.fq --dta -p 16 -S paired.sam
samtools view -@ 16 -uhS paired.sam | samtools sort -@ 16 -o sorted_paired.bam

