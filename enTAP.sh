#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 5
#SBATCH --mem=40G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load interproscan/5.25-64.0  

/labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/EnTAP  --paths /labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/entap_config.txt --runP -i /projects/EBP/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/new_annotation/EnTAP/genes.fasta.faa -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd  --qcoverage 50 --tcoverage 50  --threads 12 --ontology 0 --ontology 1 --contam fungi --contam bacteria --contam insecta --contam amoeba


