#!/bin/bash
# Submission script for Xanadu
#SBATCH --job-name=repeatmasker
#SBATCH -o repeatmasker-%j.output
#SBATCH -e repeatmasker-%j.error
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=50G
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
# Run the program                 

module load RepeatMasker/4.0.6

RepeatMasker final_assembly.fa -dir /projects/EBP/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/new_annotation/RepeatMasker/ -lib /projects/EBP/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/new_annotation/RepeatMasker/consensi.fa.classified -pa 4 -gff -a -noisy -low -xsmall| tee output_P.africana.masked.fasta

